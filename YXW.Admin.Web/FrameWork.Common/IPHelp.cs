﻿using System;

namespace FrameWork.Common
{
    public class IPHelp
    {
        /// <summary>
        /// 获取客户端Ip
        /// 
        /// </summary>
        /// <returns></returns>
        public static String GetClientIp()
        {
            String clientIP = "127.0.0.1";
            if (System.Web.HttpContext.Current != null)
            {
                clientIP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(clientIP) || (clientIP.ToLower() == "unknown"))
                {
                    clientIP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_REAL_IP"];
                    if (string.IsNullOrEmpty(clientIP))
                    {
                        clientIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                    }
                }
                else
                {
                    try
                    {
                        clientIP = clientIP.Split(',')[0];
                    }
                    catch (Exception)
                    {

                    }

                }
            }

            if (!String.IsNullOrEmpty(clientIP) && "::1".Equals(clientIP))
            {
                clientIP = "127.0.0.1";
            }
            return clientIP;
        }
    }
}