﻿/************************************************************************************
 *      Copyright (C) 2015 yuwei,All Rights Reserved
 *      File:
 *                AdminControllerBase.cs
 *      Description:
 *            AdminControllerBase
 *      Author:
 *                yxw
 *                
 *                
 *      Finish DateTime:
 *                2017/12/4 14:58:51
 *      History:
 ***********************************************************************************/


using System.Web.Mvc;
using FrameWork.Interface;

namespace FrameWork.Web
{
    /// <summary>
    /// AdminControllerBase
    /// </summary>
    public abstract class AdminControllerBase : Controller
    {
        /// <summary>
        /// 实例化IAccountService
        /// </summary>
        protected IAccountService AccountService => ServiceHelper.CreateService<IAccountService>();

        /// <summary>
        /// 记录日志service实体
        /// </summary>
        protected ILogService LogService => ServiceHelper.CreateService<ILogService>();
    }
}